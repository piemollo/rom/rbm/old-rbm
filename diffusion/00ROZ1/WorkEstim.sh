#!/bin/bash
ffpath=$HOME"/Prog/FreeFem-sources/bin/FreeFem++-mpi"

FreeFem++ MeshGen.edp -n 5 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 100 -exa 1;

FreeFem++ MeshGen.edp -n 30 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 100 -exa 1;

FreeFem++ MeshGen.edp -n 60 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 100 -exa 1;

FreeFem++ MeshGen.edp -n 100 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 10 ;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 50 ;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 100 -exa 1;
mpirun -np 1 $ffpath SHeat_coercive.edp -ne -km 500 ;
