''' #==================================================================# '''
''' |                 Error plot                                       | '''
''' #==================================================================# '''

''' ==== Loading '''
from numpy import *
from pylab import *
er1 = loadtxt('./GDY/Err21988x100.dat');
es1 = loadtxt('./GDY/EErr21988x100.dat');
er2 = loadtxt('./GDY/Err21988x225.dat');
es2 = loadtxt('./GDY/EErr21988x225.dat');
er3 = loadtxt('./GDY/Err21988x400.dat');
es3 = loadtxt('./GDY/EErr21988x400.dat');
er4 = loadtxt('./GDY/Err21988x625.dat');
es4 = loadtxt('./GDY/EErr21988x625.dat');


# ------ CV only
fig=figure(1)
clf()
n1=len(er1);
x1=range(1,n1+1)
n2=len(er2);
x2=range(1,n2+1)
n3=len(er3);
x3=range(1,n3+1)
n4=len(er4);
x4=range(1,n4+1)

semilogy(x1, er1)
semilogy(x1, es1,'--')
semilogy(x2, er2)
semilogy(x2, es2,'--')
semilogy(x3, er3)
semilogy(x3, es3,'--')
semilogy(x4, er4)
semilogy(x4, es4,'--')

semilogy([1,3],[1e-8,1e-8],'k:',linewidth=1)

'''title('Reduced basis error');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['Erreur I=100','Estim. I=100',\
    'Erreur I=225','Estim. I=225',\
    'Erreur I=400','Estim. I=400',\
    'Erreur I=625','Estim. I=625',\
    'Threshold']);
grid(linestyle='-');

fig.savefig('./graph/RbSampleSens.png');

show();
