''' #==================================================================# '''
''' |                   Constant approximation                         | '''
''' #==================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
alpt  = loadtxt('./EstimValidation/ExactAlpha21988.dat');
alp = alpt[:,0];
mu  = alpt[:,1];
alpub = loadtxt('./EstimValidation/UBAlpha21988.dat');
alplb = loadtxt('./EstimValidation/LBAlpha21988.dat');
ErrT  = loadtxt('./EstimValidation/ErrTot21988.dat');

''' ==== Plot '''
fig=figure(1)
clf()
grid('on');

plot(mu, alp  , 'r' )
plot(mu, alplb[4,:], ':')
plot(mu, alpub[1,:], ':')
 
xlabel('mu')
ylabel('alpha value')
legend(['Exact','Lower bound','Upper bound']);

fig.savefig('./graph/AlpLbUb.png');

show();
