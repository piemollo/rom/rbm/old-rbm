''' #==================================================================# '''
''' |                  Estimator plot                                  | '''
''' #==================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
ee = loadtxt('./GDY/EErrTot.dat');
ec = loadtxt('./GDY/ErrTot.dat');

fig=figure(1)
it=2
n=len(ee[:,1]);

plot(range(n), ee[:,it], 'r')
plot(range(n), ec[:,it], 'b')

title('Error respect with mu');
xlabel('mu')
ylabel('Error')
legend(['Estimator','Exact']);

fig.savefig("./graph/RbEstimPlot.png");
show();
