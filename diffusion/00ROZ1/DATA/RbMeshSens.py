''' #==================================================================# '''
''' |                 Error plot                                       | '''
''' #==================================================================# '''

''' ==== Loading '''
from numpy import *
from pylab import *
er1 = loadtxt('./GDY/Err50x225.dat');
es1 = loadtxt('./GDY/EErr50x225.dat');
er2 = loadtxt('./GDY/Err1988x225.dat');
es2 = loadtxt('./GDY/EErr1988x225.dat');
er3 = loadtxt('./GDY/Err7842x225.dat');
es3 = loadtxt('./GDY/EErr7842x225.dat');
er4 = loadtxt('./GDY/Err21988x225.dat');
es4 = loadtxt('./GDY/EErr21988x225.dat');


# ------ CV only
fig=figure(1)
clf()
n1=len(er1);
x1=range(1,n1+1)
n2=len(er2);
x2=range(1,n2+1)
n3=len(er3);
x3=range(1,n3+1)
n4=len(er4);
x4=range(1,n4+1)

semilogy(x1, er1)
semilogy(x1, es1,'--')
semilogy(x2, er2)
semilogy(x2, es2,'--')
semilogy(x3, er3)
semilogy(x3, es3,'--')
semilogy(x4, er4)
semilogy(x4, es4,'--')

semilogy([1,4],[1e-8,1e-8],'k:',linewidth=1)

'''title('Reduced basis error');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['Erreur nt=50','Estim. nt=50',\
    'Erreur nt=1988','Estim. nt=1988',\
    'Erreur nt=7842','Estim. nt=7842',\
    'Erreur nt=21988','Estim. nt=21988',\
    'Threshold']);
grid(linestyle='-');

fig.savefig('./graph/RbMeshSens.png');

show();
