''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err = loadtxt('./EstimValidation/ErrTot21988.dat');
aplt= loadtxt('./EstimValidation/ExactAlpha21988.dat');
mu = aplt[:,1];

fig=figure(1)
clf()

semilogy(mu,err[0,:]);
semilogy(mu,err[1,:]);
semilogy(mu,err[2,:]);

xlabel("mu")
ylabel("Erreur")
legend(['N=1','N=2','N=3'],
        bbox_to_anchor=(1.05, 1.0), 
        loc='upper left');
tight_layout();
grid(linestyle='-');

fig.savefig('./graph/EstimError.png');
show();
