''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err0 = loadtxt('./EstimValidation/EtaEst21988x10.dat');
err1 = loadtxt('./EstimValidation/EtaEst21988x50.dat');
err20= loadtxt('./EstimValidation/EtaEst21988x99.dat');
err2 = loadtxt('./EstimValidation/EtaEst21988x100.dat');
err21= loadtxt('./EstimValidation/EtaEst21988x101.dat');
err3 = loadtxt('./EstimValidation/EtaEst21988x500.dat');

fig=figure(1)
clf()

semilogy(range(1,len(err0)+1 ), err0)
semilogy(range(1,len(err1)+1), err1)
semilogy(range(1,len(err20)+1), err20)
semilogy(range(1,len(err2)+1), err2)
semilogy(range(1,len(err21)+1), err21)
semilogy(range(1,len(err3)+1), err3)
grid(linestyle='-');

'''title('Reduced basis error');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['I=10','I=50','I=99','I=100','I=101','I=500']);

fig.savefig('./graph/EstimSampleSens.png');
show();
