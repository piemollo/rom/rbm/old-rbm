''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err3 = loadtxt('./EstimValidation/EtaEst21988x50.dat');

fig=figure(1)
clf()

semilogy(range(1,len(err3)+1), err3)
grid(linestyle='-');

xlabel("Nombre d'éléments")
ylabel("Erreur")

fig.savefig('./graph/EstimCV.png');

fig2=figure(2);
clf()

loglog(log10(range(1,len(err3)+1)), err3)
# ex = log10(range(1,len(err3)+1));
# ey = log10(err3);
# p = polyfit(ex,ey,1);
# print("Order : ", p[0]);
# 
# x=linspace(2,len(err3),200);
# y=10**(polyval(p,log10(x)));
# loglog(x,y,'--');

xlabel("Nombre d'éléments (log)")
ylabel("Erreur")
legend(["Estimateur","Interpolée"]);

fig2.savefig('./graph/EstimCVlog.png');

show();
