''' #==================================================================# '''
''' |                 Error plot                                       | '''
''' #==================================================================# '''

''' ==== Loading '''
from numpy import *
from pylab import *
err = loadtxt('./GDY/Err.dat');
est = loadtxt('./GDY/EErr.dat');
errN= loadtxt('./GDY/ErrNON.dat');
estN= loadtxt('./GDY/EErrNON.dat');

# ------ CV only
fig=figure(1)
clf()
n=len(err);
m=len(errN);

semilogy(range(1,n+1), err, 'r')
semilogy(range(1,n+1), est, 'b')


'''title('Reduced basis error');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['Erreur EF','Estimateur']);
grid(linestyle='-');

fig.savefig('./graph/RbErrorPlot.png');

# ------ Compare with non-orthonorm
fig=figure(2)
clf()
n=len(err);

semilogy(range(1,n+1), err, 'r')
semilogy(range(1,n+1), est, 'b')
semilogy(range(1,m+1), errN, ':')
semilogy(range(1,m+1), estN, ':')


'''title('Reduced basis error');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['Erreur avec ON','Estimateur avec ON',\
    'Erreur sans ON','Estimateur sans ON']);
grid(linestyle='-');


show();
