''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err0 = loadtxt('./EstimValidation/EtaEst50x50.dat');
err1 = loadtxt('./EstimValidation/EtaEst1988x50.dat');
err2 = loadtxt('./EstimValidation/EtaEst7842x50.dat');
err3 = loadtxt('./EstimValidation/EtaEst21988x50.dat');

fig=figure(1)
clf()

semilogy(range(len(err0)), err0)
semilogy(range(len(err1)), err1)
semilogy(range(len(err2)), err2)
semilogy(range(len(err3)), err3)
grid(linestyle='-');

xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['nt=50','nt=1988','nt=7842','nt=21988']);

fig.savefig('./graph/EstimMeshSens.png');

show();
