''' #==================================================================# '''
''' |                 Error plot                                       | '''
''' #==================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
alpt = loadtxt('./EstimValidation/ExactAlpha21988.dat');
alp = alpt[:,0];
mu  = alpt[:,1];

fig=figure(1)
clf()

plot(mu, alp);

xlabel("mu")
ylabel("Valeur beta")
grid(linestyle='-');

fig.savefig('./graph/AlpPlot.png');

show();
