#!/bin/bash

FreeFem++ MeshGen.edp -n 5 -ne ;
FreeFem++ SetMatrix.edp -ne ;
FreeFem++ SHeat_greedy.edp -ne -km 15 -gen 1 -mi 50 -si 225

FreeFem++ MeshGen.edp -n 30 -ne ;
FreeFem++ SetMatrix.edp -ne ;
FreeFem++ SHeat_greedy.edp -ne -km 15 -gen 1 -mi 1988 -si 225

FreeFem++ MeshGen.edp -n 60 -ne ;
FreeFem++ SetMatrix.edp -ne ;
FreeFem++ SHeat_greedy.edp -ne -km 15 -gen 1 -mi 7842 -si 225

FreeFem++ MeshGen.edp -n 100 -ne ;
FreeFem++ SetMatrix.edp -ne ;
FreeFem++ SHeat_greedy.edp -ne -km 10 -gen 1 -mi 21988 -si 100
FreeFem++ SHeat_greedy.edp -ne -km 15 -gen 1 -mi 21988 -si 225
FreeFem++ SHeat_greedy.edp -ne -km 20 -gen 1 -mi 21988 -si 400
FreeFem++ SHeat_greedy.edp -ne -km 25 -gen 1 -mi 21988 -si 625
