#!/bin/bash
ffpath="/home/pierre/Prog/FreeFem-sources/bin/FreeFem++-mpi"

FreeFem++ MeshGen.edp -n 5 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 50 ;
FreeFem++ Greedy.edp -ne -km 50 -gen 1 -mi 110 -si 200

FreeFem++ MeshGen.edp -n 30 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 50  ;
FreeFem++ Greedy.edp -ne -km 50 -gen 1 -mi 4022 -si 200

FreeFem++ MeshGen.edp -n 60 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 50  ;
FreeFem++ Greedy.edp -ne -km 50 -gen 1 -mi 16180 -si 200

FreeFem++ MeshGen.edp -n 100 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 50;
FreeFem++ Greedy.edp -ne -km 25  -gen 1 -mi 45546 -si 100;
FreeFem++ Greedy.edp -ne -km 50  -gen 1 -mi 45546 -si 200;
FreeFem++ Greedy.edp -ne -km 100 -gen 1 -mi 45546 -si 400;
FreeFem++ Greedy.edp -ne -km 200 -gen 1 -mi 45546 -si 800;
