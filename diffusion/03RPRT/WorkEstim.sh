#!/bin/bash
ffpath="/home/mollo/Prog/FreeFem-sources/bin/FreeFem++-mpi"

FreeFem++ MeshGen.edp -n 5 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 100 -exa 1;

FreeFem++ MeshGen.edp -n 30 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 50 ;
mpirun -np 1 $ffpath SCM.edp -ne -km 100 -exa 1 ;
mpirun -np 1 $ffpath SCM.edp -ne -km 150 -Pmax 50 ;
mpirun -np 1 $ffpath SCM.edp -ne -km 200 -Pmax 50 ;


FreeFem++ MeshGen.edp -n 60 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne ;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne ;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 100 -exa 1;

FreeFem++ MeshGen.edp -n 100 -ne ;
FreeFem++ EIM.edp -n -fct 1 -eps 1e-9 -ne ;
FreeFem++ EIM.edp -n -fct 2 -eps 1e-9 -ne ;
FreeFem++ EIM.edp -n -fct 3 -eps 1e-9 -km 50 -ne ;
FreeFem++ SetMatrix.edp -ne ;
mpirun -np 1 $ffpath SCM.edp -ne -km 100 -exa 1 ;
