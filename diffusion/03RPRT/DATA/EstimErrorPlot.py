''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err = loadtxt('./EstimValidation/ErrTot45546.dat');
aplt= loadtxt('./EstimValidation/ExactAlpha45546.dat');
mu = aplt[:,1];

fig=figure(1)
clf()

semilogy(mu,err[0,:]);
semilogy(mu,err[int(len(err)/3),:]);
semilogy(mu,err[int(len(err)/3*2),:]);



xlabel("mu")
ylabel("Erreur")
legend(['N={}'.format(1),'N={}'.format(int(len(err))/3),'N={}'.format(int(len(err))/3*2)],
        bbox_to_anchor=(1.05, 1.0), 
        loc='upper left');
tight_layout();
grid(linestyle='-');

fig.savefig('./graph/EstimError.png');
show();
