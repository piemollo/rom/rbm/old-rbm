''' #==================================================================# '''
''' |                 Error plot                                       | '''
''' #==================================================================# '''

''' ==== Loading '''
from numpy import *
from pylab import *
mu1 = loadtxt('./GDY/mu21988x100.dat');
mu2 = loadtxt('./GDY/mu21988x225.dat');
mu3 = loadtxt('./GDY/mu21988x400.dat');
mu4 = loadtxt('./GDY/mu21988x625.dat');

# ------ CV only
fig=figure(1)
clf()

plot(mu1[1:,0],mu1[1:,1],'*');
plot(mu2[1:,0],mu2[1:,1],'*');
plot(mu3[1:,0],mu3[1:,1],'*');
plot(mu4[1:,0],mu4[1:,1],'*');

'''title('Reduced basis muror');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['Erreur I=100','Estim. I=100',\
    'Erreur I=225','Estim. I=225',\
    'Erreur I=400','Estim. I=400',\
    'Erreur I=625','Estim. I=625',\
    'Threshold']);
grid(linestyle='-');

fig.savefig('./graph/RbUsedMu.png');

show();
