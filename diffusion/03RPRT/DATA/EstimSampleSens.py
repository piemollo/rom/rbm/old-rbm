''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err0 = loadtxt('./EstimValidation/EtaEst4022x50.dat');
err1 = loadtxt('./EstimValidation/EtaEst4022x100.dat');
err2 = loadtxt('./EstimValidation/EtaEst4022x150.dat');
err3 = loadtxt('./EstimValidation/EtaEst4022x200.dat');

fig=figure(1)
clf()

semilogy(range(1,len(err0)+1 ), err0)
semilogy(range(1,len(err1)+1), err1)
semilogy(range(1,len(err2)+1), err2)
semilogy(range(1,len(err3)+1), err3)
grid(linestyle='-');

'''title('Reduced basis error');'''
xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['I=50','I=100','I=150','I=200']);

fig.savefig('./graph/EstimSampleSens.png');
show();
