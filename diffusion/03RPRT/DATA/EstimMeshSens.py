''' #=================================================================# '''
''' |                Error plot                                       | '''
''' #=================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
err0 = loadtxt('./EstimValidation/EtaEst110x100.dat');
err1 = loadtxt('./EstimValidation/EtaEst4022x100.dat');
err2 = loadtxt('./EstimValidation/EtaEst16180x100.dat');
err3 = loadtxt('./EstimValidation/EtaEst45546x100.dat');

fig=figure(1)
clf()

semilogy(range(len(err0)), err0)
semilogy(range(len(err1)), err1)
semilogy(range(len(err2)), err2)
semilogy(range(len(err3)), err3)
grid(linestyle='-');

xlabel("Nombre d'éléments")
ylabel("Erreur")
legend(['nt=110','nt=4022','nt=16180','nt=45546']);

fig.savefig('./graph/EstimMeshSens.png');

show();
