''' #==================================================================# '''
''' |                 Error plot                                       | '''
''' #==================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
alpt = loadtxt('./EstimValidation/ExactAlpha110.dat');
alp0=alpt[:,0];
mu0 =alpt[:,1];

alpt = loadtxt('./EstimValidation/ExactAlpha4022.dat');
alp1=alpt[:,0];
mu1 =alpt[:,1];

alpt = loadtxt('./EstimValidation/ExactAlpha16180.dat');
alp2=alpt[:,0];
mu2 =alpt[:,1];

alpt = loadtxt('./EstimValidation/ExactAlpha45546.dat');
alp3=alpt[:,0];
mu3 =alpt[:,1];

fig1=figure(1)
clf()

plot(mu0, alp0);
plot(mu1, alp1);
plot(mu2, alp2);
plot(mu3, alp3);

'''title('Reduced basis error');'''
xlabel("mu")
ylabel("Valeur de beta")
legend(['nt=110','nt=4022','nt=16180','nt=45546']);
grid(linestyle='-');

fig1.savefig('./graph/AlpMeshSens1.png');

fig2=figure(2)
clf()
dif0=abs(alp0-alp3);
dif1=abs(alp1-alp3);
dif2=abs(alp2-alp3);

semilogy(mu1, dif0);
semilogy(mu1, dif1);
semilogy(mu1, dif2);

'''title('Reduced basis error');'''
xlabel("mu")
ylabel("Difference")
legend(['nt=50','nt=1988','nt=7842']);
grid(linestyle='-');

fig2.savefig('./graph/AlpMeshSens2.png');

show();
