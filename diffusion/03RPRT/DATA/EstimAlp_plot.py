''' #==================================================================# '''
''' |                   Constant approximation                         | '''
''' #==================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
alpt  = loadtxt('./EstimValidation/ExactAlpha45546.dat');
alp = alpt[:,0];
mu  = alpt[:,1];
alpub = loadtxt('./EstimValidation/UBAlpha45546.dat');
alplb = loadtxt('./EstimValidation/LBAlpha45546.dat');
ErrT  = loadtxt( './EstimValidation/ErrTot45546.dat');

''' ==== Plot '''
fig=figure(1)
clf()
grid('on');

plot(mu, alp  , 'r' )
plot(mu, alplb[4,:], ':')
plot(mu, alpub[1,:], ':')
 
xlabel('mu')
ylabel('alpha value')
legend(['Exact','Lower bound','Upper bound']);

fig.savefig('./graph/AlpLbUb.png');

show();
