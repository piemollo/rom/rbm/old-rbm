//#==========================================================================#
//|               Mesh generator : 2D step                                   |
//#==========================================================================#

// ------ ------ Parameters

// ------ Dimensions
int n = 10;		//number of nodes by unit

// ------ Labels
int input  = 1;		//left side
int output = 2;		//right side
int side   = 3;	        //borders
int inter  = 4;

// ------ Variables
string filename = "";	//save name
mesh T1, T2, Th;	//mesh

// ------ ------ Mesh

// ------ Ref points
real[int,int] Pt(7,2);
Pt(0,0) =-1.0; Pt(0,1) = 1.0;
Pt(1,0) = 4.0; Pt(1,1) = 1.0;
Pt(2,0) = 4.0; Pt(2,1) = 0.0;
Pt(3,0) = 4.0; Pt(3,1) =-1.0;
Pt(4,0) = 0.0; Pt(4,1) =-1.0;
Pt(5,0) = 0.0; Pt(5,1) = 0.0;
Pt(6,0) =-1.0; Pt(6,1) =-0.0;

// ------ Borders
border b1(t=0., 1.){x=(Pt(1,0)-Pt(0,0))*t + Pt(0,0);
		    y=(Pt(1,1)-Pt(0,1))*t + Pt(0,1);
		label=side ;};                  
border b2(t=0., 1.){x=(Pt(2,0)-Pt(1,0))*t + Pt(1,0);
		    y=(Pt(2,1)-Pt(1,1))*t + Pt(1,1);
		label=output;};                  
border b3(t=0., 1.){x=(Pt(3,0)-Pt(2,0))*t + Pt(2,0);
		    y=(Pt(3,1)-Pt(2,1))*t + Pt(2,1);
		label=output;};                  
border b4(t=0., 1.){x=(Pt(4,0)-Pt(3,0))*t + Pt(3,0);
		    y=(Pt(4,1)-Pt(3,1))*t + Pt(3,1);
		label=side;};                  
border b5(t=0., 1.){x=(Pt(5,0)-Pt(4,0))*t + Pt(4,0);
		    y=(Pt(5,1)-Pt(4,1))*t + Pt(4,1);
		label=side;};                  
border b6(t=0., 1.){x=(Pt(6,0)-Pt(5,0))*t + Pt(5,0);
		    y=(Pt(6,1)-Pt(5,1))*t + Pt(5,1);
		label=side;};
border b7(t=0., 1.){x=(Pt(0,0)-Pt(6,0))*t + Pt(6,0);
		    y=(Pt(0,1)-Pt(6,1))*t + Pt(6,1);
		label=input;};
border b8(t=0., 1.){x=(Pt(5,0)-Pt(2,0))*t + Pt(2,0);
		    y=(Pt(5,1)-Pt(2,1))*t + Pt(2,1);
		label=inter;};


// ------ Gen
T1 = buildmesh( b1(-5*n) + b2(-n) + b8(-4*n) + b6(-n) + b7(-n) ) ;
T2 = buildmesh( b8(4*n) + b3(-n) + b4(-4*n) + b5(-n) );
int[int] re =[0,1];
T2=change(T2, region=re);
Th = T1 + T2;


// ------ Save
filename = "./mesh/";
savemesh(Th, filename + "Th.msh");
savemesh(T1, filename + "th1.msh");
savemesh(T2, filename + "th2.msh");

// ------ Display
plot(Th, cmm = "Number of triangles = " + Th.nt );

fespace Xh(Th, P2);
Xh u;

fespace Qh(Th, P1);
Qh v;

cout << "Nb DOF for P1 : " ;
cout << v.n << endl; 

cout << "Nb DOF for P2 : " ;
cout << u.n << endl; 
