''' #===================================================================# '''
''' |                  Error plot                                       | '''
''' #===================================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *
''' Mono param '''
e1 = loadtxt('./MonoParam/CGdy/Err.dat'); ''' Compact '''
e2 = loadtxt('./MonoParam/SGdy/Err.dat'); ''' Supremized '''
''' Multi param '''
e3 = loadtxt('./MultiParam/CGdy/Err.dat');''' Compact '''
et3= loadtxt('./MultiParam/CGdy/ErrTot.dat');
e4 = loadtxt('./MultiParam/SGdy/Err.dat');''' Supremized '''
et4= loadtxt('./MultiParam/SGdy/ErrTot.dat');

''' ==== Sizes '''
N = zeros([4]);
N[0] = len(e1); 
N[1] = len(e2); 
N[2] = len(e3); 
N[3] = len(e4); 
print(N)

''' ==== Means '''
em3 = mean(et3,axis=0);
em4 = mean(et4,axis=0);

''' ==== Plot mono '''
figure(1)
clf()

#plot(log10(range(1,int(N[0]+1))), log10(e1));
#plot(log10(range(1,int(N[1]+1))), log10(e2));

plot((range(1,int(N[0]+1))), log10(e1));
plot((range(1,int(N[1]+1))), log10(e2));

legend(['CGdy (Emax)','SGdy (Emax)']);
title('Errors evolution for Mono-parameter (log)');
xlabel('Size of RB ');
ylabel('Error (log)');

''' ==== Plot multi '''
figure(2)

#plot(log10(range(1,int(N[2]+1))), log10(e3));
#plot(log10(range(1,int(N[2]+1))), log10(em3));
#plot(log10(range(1,int(N[3]+1))), log10(e4));
#plot(log10(range(1,int(N[3]+1))), log10(em4));

plot((range(1,int(N[2]+1))), log10(e3));
plot((range(1,int(N[2]+1))), log10(em3));
plot((range(1,int(N[3]+1))), log10(e4));
plot((range(1,int(N[3]+1))), log10(em4));

legend(['CGdy (Emax)','CGdy (Emean)','SGdy (Emax)','SGdy (Emean)']);
title('Errors evolution for Multi-parameter (log)');
xlabel('Size of RB ');
ylabel('Error (log)');


show();
