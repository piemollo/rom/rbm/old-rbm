macro def(u)[u, u#B, u#C]// EOM
macro init(u)[u, u, u]// EOM
macro grad(u)[dx(u), dy(u)]// EOM // two-dimensional gradient
macro div(u)(dx(u) + dy(u#B))// EOM
func Pk = [P2, P2, P1];
int NbEV = 2;

mesh Th;
if(!mpirank) Th = readmesh("./mesh/Th.msh");
broadcast(processor(0),Th);
macro ON() + on(0,2, u=0, uB=0);//

fespace Vh(Th, Pk);         // local FE
buildDmesh(Th)
Mat A, M;
createMat(Th, M, Pk);
createMat(Th, A, Pk);

varf vPbA([u, uB, uC], [v, vB, vC]) = 
int2d(Th)(grad(u)' * grad(v) + grad(uB)' * grad(vB)
- div(u) * vC - div(v)*uC ) ON;
varf vPbM([u,uB,uC],[v,vB,vC]) = int2d(Th)( -uC * vC) ON;

matrix Aff = vPbA(Vh,Vh, tgv=-2);
Aff.thresholding(1e-30);
A = Aff;
matrix Mff = vPbM(Vh,Vh,tgv=-2);
Mff.thresholding(1e-30);
M = Mff;
int nVh;
real[int,int] out(Vh.ndof,NbEV);

Vh def(u);
ChangeNumbering(A, u[], out(:,0));

real[int] nev(NbEV);
real[int,int] neV(Vh.ndof,NbEV);
string MinParam=""
+ "-eps_gen_non_hermitian "
+ "-st_pc_type lu "
+ "-st_ksp_type gmres "
+ "-st_ksp_rtol 1e-6 "
+ "-eps_target_magnitude "
+ "-st_type sinvert "
+ "-eps_target 1.0e-5 "
+ "-eps_nev "+NbEV+" ";
if(0){
	MinParam = MinParam
				+ "-eps_error_relative ::ascii_info_detail "
				+ "-st_ksp_converged_reason ";
}			

EPSSolve(A, M, sparams=MinParam, values=nev, array=out);

//ObjectView(A, format="matlab", name="LHS2.m");
//ObjectView(M, format="matlab", name="RHS2.m");

if(!mpirank){
	for(int q=0; q<NbEV; q++) cout << nev[q] << endl;
}

// ChangeNumbering(A,u[],out(:,0),inverse=true,exchange=true);
// real[int] w = A*u[];
// real vall = u[]'*w;
// w = M*u[];
// vall /= u[]'*w;
// if(!mpirank) cout << "test vector: " << vall << endl;
