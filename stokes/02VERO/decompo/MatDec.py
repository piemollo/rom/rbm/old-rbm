from numpy import *

''' Mesh '''
nbt = 7


''' Settings '''
NbParam = 2
nu = 4.02e-3

Qa = 3*nbt
DifTypeA = 3

Qb = 4*nbt
DifTypeB = 4

Qf = 0
DifTypeF = 0

Qg = 0
DifTypeG = 0

Settings = [ [NbParam, nu], [Qa, DifTypeA], [Qb, DifTypeB], \
        [Qf, DifTypeF], [Qg, DifTypeG]]

''' A regions '''
A = zeros([nbt,DifTypeA])
for i in range(nbt):
    for j in range(DifTypeA):
        A[i,j] = j + i*DifTypeA

''' B regions '''
B = zeros([nbt,DifTypeB])
for i in range(nbt):
    for j in range(DifTypeB):
        B[i,j] = j + i*DifTypeB

''' Save '''

f = open("setting.rb",'w')
f.write("{:d} {:f}\n".format(NbParam,nu))
for i in range(1,5):
    f.write("{:d} {:d}\n".format(Settings[i][0], Settings[i][1]))
f.close()

savetxt("DecA.rb", A, fmt='%.d')
savetxt("DecB.rb", B, fmt='%.d')
