//#=============================================================#
//|          Constructor  : Reduced basis (with ortho-greedy)   |
//#=============================================================#

// ====== ====== Initialization ====== ======
verbosity = 0;
cout.precision(3);
include "./../../COMMON/RBfunction.idp"
include "./../../COMMON/RBassembler.idp"
load "PETSc"
load "SLEPc"
include "getARGV.idp"

macro mout()if(!mpirank) cout //
macro tic()TIC=mpiWtime()//
macro toc()TOC=mpiWtime()-TIC//
macro time()"("+TOC+"s)"//
real TIC, TOC; // ... on the clock

// ------ Title
if(!mpirank){
	cout << "#===== ====== ====== ====== =====#" << endl;
	cout << "|         Natural-Norm           |" << endl;
	cout << "|  Successive Constraint Meth.   |" << endl;
	cout << "#===== ====== ====== ====== =====#" << endl;
	cout << "| #### Initialization " << endl;
}

// ------ Control
int plt = getARGV("-plt",0);	// Display details ? ( no=0, yes=1 )
real threshold = 1e-5;
string ExName = "./";
string SubName = "TestSCM";

// ------ Mesh
tic;
mesh Th;
Th = readmesh(ExName+"mesh/Th.msh");
//buildDmesh(Th);
toc;
fespace Yh(Th,[P2,P2,P1]);
mout << "|  # Load and distribute mesh "+time << endl;

// ------ ------ Decompositions
tic;
int NbParam, Qa, Qb;
matrix BCE, SPh, SPUh, SPPh;
matrix[int] Ah(0), Bh(0);
LoadEVmatrices(ExName, NbParam, Qa, Qb, Ah, Bh, BCE, SPh, SPUh, SPPh);
toc;

mout << "|  # Load matrices "+time << endl;

// --- Load in PETSc
tic;
Mat[int] Ahp(Qa+Qb);
// A part
for(int q=0; q<Qa; q++){
	Mat MatTmp(Ah[q]); // can't directly set Mat[int] = matrix
	Ahp[q] = MatTmp;
}
// B+B' part
for(int q=0; q<Qb; q++){
	matrix matrixTmp = Bh[q] + Bh[q]';
	Mat MatTmp(matrixTmp);
	Ahp[q+Qa] = MatTmp;
}

toc;
mout << "|  # Import matrices in PETSc "+time << endl;

// ------ Discret mu space 
tic;
int 	 DkTot=1;							// Size of total D space
int[int] Dk(NbParam);						// Sizes  of sub-D spaces
real[int] Dkmax(NbParam), Dkmin(NbParam);	// Bounds of sub-D spaces
int[int] SubDk(NbParam);					// Indices for sub-D spaces
real[int] mu(NbParam), mut(NbParam);		// mu variables

{ifstream ifl(ExName+"cases/"+SubName+"/MuSpaceSetting.rb");
	// Check Nb of params
	int TmpNbParam;
	ifl >> TmpNbParam;
	if(TmpNbParam!=NbParam){
		mout << "| /!/ WARNING : conflict with MuSpaceSetting.rb";
		mout << endl;
	}
	
	// Sizes of sub-D spaces
	for(int q=0; q<NbParam; q++)
		ifl >> Dk[q];
	
	// Bounds of sub-D spaces
	for(int q=0; q<NbParam; q++){
		ifl >> Dkmin[q];
		ifl >> Dkmax[q];
	}
}
// --- Size of the D space
for(int i=0; i<NbParam; i++) DkTot *= Dk[i];
if(DkTot==1) 
	mout << "| /!/ WARNING : size of D space = 1\n| " << endl;

// --- Fill the D space
real[int, int] 	Dmu(Dk.max, NbParam);
Dmu=0.;

for(int q=0; q<NbParam; q++){
	if(Dk[q]==1){
		Dmu(0,q)  = Dkmin[q];
	}else{
		for(int k=0; k<Dk.max; k++)
			Dmu(k,q) = (Dkmax[q]-Dkmin[q])/(Dk[q]-1) * k + Dkmin[q];
	}
}


// --- Assembling coefficient (theta mu)
include "MuDec.idp";
real[int,int] ca(DkTot,Qa);
real[int,int] cb(DkTot,Qb);
SubDk=0.; SubDk[0]=-1;
for(int K=0; K<DkTot; K++){
	SubDk[0]++;
	for(int ki=0; ki<NbParam; ki++){
		if(SubDk[ki]==Dk[ki]){ 
			SubDk[ki+1]++;
			SubDk[ki] = 0.;
		}
		mu[ki] = Dmu(SubDk[ki],ki);
	}
	real[int] CA(Qa), CB(Qb), CF(0), Cg(0);
	MuDec(mu, CA, CB, CF, Cg);
	ca(K,:) = CA;
	cb(K,:) = CB;
}

// --- Qab 
int Qab=Qa+Qb;
real[int,int] cab(ca.n,ca.m+cb.m);
for(int q=0 ; q<Qa ; q++) cab(:,q) = ca(:,q);
for(int q=Qa; q<Qab; q++) cab(:,q) = cb(:,q-Qa);
toc;
mout << "|  # Compute decomposition coef. "+time << endl;
mout << "| " << endl;

// ------ Parameters
// --- Control
int Pmax = 30;			// Maximum size for RB
int munp;				// Starting point & indice mu_n+1
int[int] muc(Pmax);		// Choosen mu
int I=0;				// Main iteration var. for SCM
int cv=0;				// Stop var.
real M;					// |
munp = 0;
muc  = munp;
real[int] alp(DkTot); 		alp=0.;		// Exact value


// ====== ====== Eigen problem settings ====== ======

// ------ Option & display
bool PETScEigInfo = usedARGV("-EVinfo") != -1;
bool SetUBbox = usedARGV("-SetBox") != -1;
int PSmatrix = getARGV("-PSmat",1);
mout << "| #### Eigen problem settings" << endl;
mout << "|  # Display => " << PETScEigInfo << endl;
mout << "|  # Set box => " << SetUBbox << endl;

// ------ RHS matrix
matrix EMPTY(Yh.ndof);
Mat RHS(EMPTY);
if(PSmatrix==0){
	// Full sp matrix
	mout << "|  # Scaral prod. => Full" << endl;
	if(!SetUBbox) SPh = -1*SPh;
	Mat TMP(SPh);
	RHS += TMP;
}else{
	// Pressure-only sp matrix
	mout << "|  # Scaral prod. => pressure only" << endl;
	SPPh = -1*SPPh;
	SPPh = SPPh + BCE;
	Mat TMP(SPPh);
	RHS += TMP;
}

// ------ Common EV parameters
real[int] nev(1);
real[int, int] neV(Yh.ndof,1);
//Yh def(upetsc);

// --- PETSc min EV settings
string MinParam=""
+ "-eps_gen_non_hermitian "
+ "-st_pc_type lu "
+ "-st_ksp_type gmres "
+ "-st_ksp_rtol 1e-6 "
+ "-eps_target_magnitude "
+ "-st_type sinvert "
+ "-eps_target 1.0e-9 "
+ "-eps_nev 1 ";

// --- PETSc max EV settings
string MaxParam=""
+ "-eps_gen_non_hermitian "
+ "-st_pc_type lu "
+ "-eps_largest_real "
+ "-eps_nev 1 ";

// --- Display option
if(PETScEigInfo){
	MaxParam = MaxParam
				+ "-eps_error_relative ::ascii_info_detail "
				+ "-st_ksp_converged_reason ";
	MinParam = MinParam
				+ "-eps_error_relative ::ascii_info_detail "
				+ "-st_ksp_converged_reason ";
}			


// ------ Exact value (for validation)
if(1){
	mout << "| #### Compute exact inf-sup :" + DkTot + "elem" << endl;
	for(int K=0; K<DkTot; K++){
 		tic;
		Mat LHS(BCE);
 		for(int q=0; q<Qa; q++) LHS += ca(K,q)*Ahp[q];
 		for(int q=0; q<Qb; q++) LHS += cb(K,q)*Ahp[q+Qa];
		
		EPSSolve(LHS, RHS, sparams=MinParam, values=nev);
 		alp[K] = nev[0];
		toc;
 		mout << "|  # Exa("+K+")="+ sqrt(nev[0]) +"   "+ time << endl;
 		//ofl << sqrt(nev[0]) << endl;
		
		//ObjectView(LHS, format="matlab", name="LHS1.m");
		//ObjectView(RHS, format="matlab", name="RHS1.m");
		
	}
	mout << "| " << endl;
}
