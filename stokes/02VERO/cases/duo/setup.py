from numpy import *

''' Mu space settings '''
NbParam = 2
Kmu = [50, 50]

''' Mu range '''
mu0 = [0.1, 0.5]
mu1 = [0.1, 0.5]


''' Save '''
f = open('MuSpaceSetting.rb', 'w')
f.write("{:d}\n".format(NbParam))
for i in range(NbParam):
    f.write("{:d} ".format(Kmu[i]))
f.write("\n")

f.write("{:f} {:f}\n".format(mu0[0], mu0[1]))
f.write("{:f} {:f}\n".format(mu1[0], mu1[1]))

f.close()
