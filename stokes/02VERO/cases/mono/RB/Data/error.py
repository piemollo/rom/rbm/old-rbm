''' #======================================================# '''
''' |               Error plot                             | '''
''' #======================================================# '''


''' ==== Loading '''
from numpy import *
from pylab import *

e1 = loadtxt('./Err.dat'); ''' Compact '''
e2 = loadtxt('./Res.dat');

''' ==== Plot mono '''
figure(1)
clf()

N1 = len(e1)
N2 = len(e2)

#plot((range(1,int(N1+1))), e1);
#plot((range(1,int(N2+1))), e2);

semilogy((range(1,int(N1+1))), e1);
semilogy((range(1,int(N2+1))), e2);

E = abs(e2[-1]/e1[-1])
print(mean(E))

legend(['Error','Residual']);
title('Errors evolution for Mono-parameter (log)');
xlabel('Size of RB ');
ylabel('Error (log)');

show();
